Katas for demos

fraud.detector: concurrent in memory cache example, java 8

orderbook: concurrent processing of orders in trading application

newTvPrograms: classic RESTfull web service example, java 8

berlin-clock: classic berlin clock kata implemented in java 8, with functional programming approach 

popular-tracks: working with json and file processing.