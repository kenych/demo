package com.katas.techtest;

import ken.kata.techtest.BerlinClock;
import ken.kata.techtest.BerlinClockImpl;
import ken.kata.techtest.parser.BerlinClockParser;
import com.katas.techtest.utils.Exceptions;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.katas.techtest.utils.TestHelper.TEST_CASE_POOL;
import static com.katas.techtest.utils.TestHelper.Tuple2;
import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
/**
 * Parameterized test that loads test cases from TEST_CASE_POOL
 */
public class BerlinClockTest {
    BerlinClock berlinClock;

    @Before
    public void setUp() {
        berlinClock = new BerlinClockImpl(new BerlinClockParser("(\\d{2})::(\\d{2}):(\\d{2})"));
    }

    @Test
    public void nullTest() {
        Exceptions.assertThat(() -> berlinClock.displayTime(null)).
                throwsException(IllegalArgumentException.class).
                withMessageContaining("Input");
    }

    @Parameters
    Tuple2[] dataProvider() {
        return TEST_CASE_POOL;
    }

    @Test
    @Parameters(method = "dataProvider")
    public void testDisplayTime(Tuple2 testCase) {
        String input = testCase.first;
        String expected = testCase.second;

        String actual = berlinClock.displayTime(input);

        assertThat(actual).overridingErrorMessage("for input: " + input +
                " expected output was: " + expected +
                " but got: " + actual).
                isEqualTo(expected);
    }
}
