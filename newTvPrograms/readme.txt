== problem ====

What’s New API

Scenario:
As a Set-top-box, I want to see a weekly list of programmes that has never aired before

The TV Services team has partnered with Broadcast Data team and asked them to provide a service (“Programmes this week API”)
that returns all the programmes that are going to be aired this week. This service also exposes a first aired date field that is of interest to determine if a programme is new.

Acceptance Criteria
1. What’s New API will be called by the set-top-box without any parameters.
2. API should return a JSON response
3. The API should return a list of programmes for this week that has never been aired
before
4. Each programme returned should have the following data
 series name
 season number
 episode name
 episode number
The Broadcast Data team are currently developing the “Programmes this week API” which does not take any parameters.
You are required to provide a mock or stub of the “Programmes this week API”

Each programme returned by the “Programmes this week API” has following data
 series name
 season number
 episode number
 episode name
 broadcast date time
 first broadcast date time
 region

== solution ===


Please make sure you have java 8 installed.

If you run: mvn test
this would start actual jetty server and test all classes, as well as main end point WhatsNewResource through
WhatsNewResourceTest which is kind of acceptance test class. That class is using RestAssured to send http request to main endpoint and then
the service will send actual http request through jersey client to hypothetical BroadcastService to retrieve
weekly programs to be filtered. As BroadcastService is a remote service, we mock it with WireMockRule in WhatsNewResourceTest.

As BroadcastServiceRestClient is connecting to remote Http service, many things can go wrong potentially, so we have thorough
tests covering most of cases like 404 and parsing json returned form remote service.
So in case remote ENDPOINT is down, or json has changed, our service would handle it gracefully and we have tests covering that.

Please also note that BroadcastServiceRestClient is using retry mechanism which also has tests covering its behaviour.

Regards,

Kayan Azimov.
